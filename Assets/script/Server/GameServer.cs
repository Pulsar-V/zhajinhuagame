﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
namespace Assets.script.Server {
    class GameServer {
        public static Socket client; 
        static IPAddress ip; 
        static IPEndPoint ipEnd;
        static string recvStr;
        static string sendStr;
        static byte[] recvData = new byte[1024]; 
        static byte[] sendData = new byte[1024];
        static int recvLen; 
        public static bool ReceiveFlag = false;
        // Use this for initialization
        public static void Start(string ipAddress,int port) {
            ip = IPAddress.Parse ( ipAddress );
            ipEnd = new IPEndPoint ( ip, port );
         Thread connectThread = new Thread ( new ThreadStart ( SocketReceive ) );
            //TODO:本地接收器
            StartGame.ADD_TO_THREAD_POOL ( "localReceive", connectThread );
            Debug.Log ( "启动线程" );
            
            connectThread.Start ( );
        }
        static void SocketConnet() {
            if (client != null)
                client.Close ( );
            client = new Socket ( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );

            //连接  
            //Debug.Log ( "链接目标服务器" );
            client.Connect ( ipEnd );
            if (client.Connected) {
                Debug.Log ( "链接成功" );
            }
            else {
                Debug.Log ( "链接失败" );
            }
        }
        static void SocketReceive() {
            SocketConnet ( );
            while (true) {
                recvData = new byte[1024];
                recvLen = client.Receive ( recvData );
                if (recvLen == 0) {
                    SocketConnet ( );
                    continue;
                }
                recvStr = Encoding.ASCII.GetString ( recvData, 0, recvLen );
                ReceiveFlag = true;
            }
        }
        public static string SocketSend(String msg) {
            byte[] data = Encoding.ASCII.GetBytes ( msg );
            int length = 256;
            int data_len = data.Length;
            client.Send ( data );
            int time = 0;
            while (!ReceiveFlag) {
                Thread.Sleep ( 1000 );
                //Debug.Log (time);
                time++;
            }
            return recvStr;
        }
        
    }
}
