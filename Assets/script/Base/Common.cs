﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

public class Common : MonoBehaviour {

    public static void ClearScenceData() {
        UnityEngine.Object[] objAry = Resources.FindObjectsOfTypeAll<Material> ( );

        for (int i = 0; i < objAry.Length; ++i) {
            objAry[i] = null;//flush resouce
        }
        UnityEngine.Object[] objAry2 = Resources.FindObjectsOfTypeAll<Texture> ( );
        for (int i = 0; i < objAry2.Length; ++i) {
            objAry2[i] = null;
        }
        //unload resource
        Resources.UnloadUnusedAssets ( );
        //flush cache
        GC.Collect ( );
        GC.WaitForPendingFinalizers ( );//waitting thread until queue messsage flush done
        GC.Collect ( );
    }
    // Use this for initialization
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {

    }
}
