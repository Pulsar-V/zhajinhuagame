﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Assets.script.Base {
    class RsaModel {
        protected static string rsaKey =( "AAAAB3NzaC1yc2EAAAADAQABAAABAQDLHoFX63UaKPKFrm5Rd2YYXviiFpJnrHwqJbiXPXgRUGshUM6vCpocbjd23i3njZkCaZm1ANCBwMkj5AuTrrMRBKkIa3vdzMUBplpmXrExTc5UvFOioRiOK+4D1L94kdhrNtlynOEW8nT65MfsQBHoD1QGJ9W6RmHEv+kEaQoYkDetc6VXBbSvFYfjwTWEGYtjoaBc5JnXdz2mdJ0VhlKVlCjYf4Bi3okK9dPuJxs5PRfGziFBUGUSE/ro+xSQbJJulS2pjihzsCQ4apV7UEj4uHCYK+aKrBKxRkg0ysEz8J83XhQ62TEehbdoxfrRwnPSvw0DNc8YKWLmDVWXzEwt" );

        public void RSAKey(out string xmlKeys, out string xmlPublicKey) {
            try {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider ( );
                xmlKeys = rsa.ToXmlString ( true );
                xmlPublicKey = rsa.ToXmlString ( false );
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        public static string RSAEncrypt( string data) {
            Func<string, byte[]> hexToBytes = (s) =>
            {
                byte[] bytes = new byte[s.Length / 2];
                for (int i = 0; i < bytes.Length; i++) {
                    bytes[i] = Convert.ToByte ( s[i * 2] + "" + s[i * 2 + 1], 16 );
                }
                return bytes;
            };

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider ( )) {
                RSAParameters rsaParams = new RSAParameters ( ) {
                    Exponent = new byte[] { 1, 0, 1 },
                    Modulus = hexToBytes ( rsaKey ),
                };
                rsa.ImportParameters ( rsaParams );

                byte[] encrypted = rsa.Encrypt ( Encoding.UTF8.GetBytes ( data ), false );
                return BitConverter.ToString ( encrypted ).Replace ( "-", "" );
            }
        }
    }
}
