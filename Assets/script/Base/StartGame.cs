﻿using Assets.script.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using UnityEngine;
using UnityEngine.Networking;

public class StartGame : MonoBehaviour {
    KeyCode currentKey;
    public static string LastUsername = null;
    public static string LastPassword = null;
    public static Dictionary<String, Thread> ThreadPool;
    public static Dictionary<String, String> Config = new Dictionary<string, string> ( );
    public static Assets.script.JsonData.JSONDATA_Client.SessionHash sessionhash;
    [RuntimeInitializeOnLoadMethod]
    static void Initialize() {
        loadConfig ( );
        //Application.Quit ( );
        ThreadPool = new Dictionary<string, Thread> ( );
        GameServer.Start (Config["server0"],int.Parse( Config["port0"]) );
        GameObject.DontDestroyOnLoad ( new GameObject ( "ClientNetwork", typeof ( StartGame ) ) {
            hideFlags = HideFlags.HideInInspector
        } );

    }
    [RuntimeInitializeOnLoadMethod]
    void OnGUI() {
        if (Input.anyKeyDown) {
            Event e = Event.current;
            if (e.isKey) {
                currentKey = e.keyCode;
                Debug.Log ( "Current Key is : " + currentKey.ToString ( ) );
            }
        }
    }
    void OnDestroy() {
        GameServer.client.Close ( );
        foreach (Thread thread in ThreadPool.Values) {
            thread.Abort ( );
        }
        Debug.Log ( name + " : OnDestroy" );
    }
    static int loadConfig() {
        string path = Application.dataPath + @"/Resources/config.xml";
        if (File.Exists ( path )) {
            XmlDocument doc = new XmlDocument ( );
            lock (doc) {
                doc.Load ( path );
            }
            XmlNodeList node = doc.SelectSingleNode ( "root" ).ChildNodes;
            foreach (XmlNode x in node) {
                switch (x.Name) {
                    case "servers":
                        XmlNodeList child = x.ChildNodes;
                        int count = 0;
                        foreach (XmlElement server in child) {
                            Config.Add ( "server" + count.ToString ( ), server.GetAttribute ( "ip" ) );
                            Config.Add ( "port" + count.ToString ( ), server.GetAttribute ( "port" ) );
                            Debug.Log ( "IP:" + server.GetAttribute ( "ip" ) + "PORT:"+ server.GetAttribute ( "port" ) );
                            count+=1;
                        }
                        break;
                    case "login":
                        XmlElement login = (XmlElement)x;
                        Config.Add ( "lastlogin_account", login.GetAttribute( "lastlogin_account" ) );
                        Config.Add ( "lastlogin_password", login.GetAttribute ( "lastlogin_password" ) );//lastlogin_password
                        Config.Add ( "autologin", login.GetAttribute ( "autologin" ) );//autologin
                        break;
                    case "effect_volume":
                        XmlElement effect_volume = (XmlElement)x;
                        Config.Add ( "effect_volume", effect_volume.GetAttribute ( "value" ) );//autologin
                        break;

                    case "bgmusic_volume":
                        XmlElement bgmusic_volume = (XmlElement)x;
                        Config.Add ( "bgmusic_volume", bgmusic_volume.GetAttribute ( "value" ) );//autologin
                        break;
                    case "version":
                        XmlElement version = (XmlElement)x;
                        Config.Add ( "version", version.GetAttribute ( "value" ) );
                        break;
                }
            }
            return 1;
        }
        else {
            return 0;
        }

    }
    public static void ADD_TO_THREAD_POOL(string name, Thread thread) {
        if (!StartGame.ThreadPool.ContainsKey ( name )) {
            StartGame.ThreadPool.Add ( name, thread );
        }
        else {
            StartGame.ThreadPool[name].Abort ( );
            StartGame.ThreadPool[name] = thread;
        }
    }
    private void Update() {
        if (GameServer.client.Connected) {

        }
        else {
            Debug.Log ( "失去连接");
        }
    }
}
