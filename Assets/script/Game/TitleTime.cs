﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleTime : MonoBehaviour {

    // Use this for initialization
    public Text title;
	void Start () {
        this.title = GameObject.Find ( "Canvas/Panel/title_time").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        System.DateTime now = System.DateTime.Now;
        title.text = now.ToLongTimeString();
	}
}
