﻿
using Assets.script.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

class Initation: MonoBehaviour {
    private void Start() {
        if (GameServer.client.Connected)
            GameServer.SocketSend ( "Initation" );
        else {
            SceneManager.LoadSceneAsync ( "Login" );
        }
    }
}
