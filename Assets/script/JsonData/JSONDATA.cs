﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.script.JsonData {
    public class JSONDATA_Client {
        [Serializable]
        public class SendDataJson<T> {
            public string server;
            public string version;
            public T data;
        }
        [Serializable]
        public class LoginData_Normal {
            public string action;
            public string type;
            public string username;
            public string password;

        }
        [Serializable]
        public class ShopRequest : SessionHash {
            private string ancion = "getshopinfo";

        }
        [Serializable]
        public class InitRequest : SessionHash {
            private string ancion = "getuserinfo";
        }
        [Serializable]
        public class SessionHash {
            public string uid;
            public string sessionhash;
        }
        [Serializable]
        public class EnterHell : SessionHash {
            public string action = "enterhell";
            public Hell hell;
        }
        [Serializable]
        public class Hell {
            public string level;
        }
        [Serializable]
        public class ServerResponse {
            public string level;
        }
    }
    class JSONDATA_Server {
        [Serializable]
        public class ReceiveData {
            int time;
        }
    }
}
