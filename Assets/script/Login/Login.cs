﻿using Assets.script.Server;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.script.JsonData;

public class Login : MonoBehaviour {
    delegate void loginController(string pressedButton);
    private bool logSuccess=false;
    private void LoginAction(string msg) {
        if (GameServer.client.Connected) {
            /*
             * {"server":"112.74.37.82","version":"1.0.0","data":
             *      {"action":"login","type":"normal","password":"admin","username":"admin"}
             *  }
             */
            GameServer.ReceiveFlag = false;
            string str = null;
            Thread loginAction = new Thread ( () => {
                string loginInfo = msg;
                str = GameServer.SocketSend ( loginInfo );
                Debug.Log ( str );
                logSuccess = true;
            } );
            StartGame.ADD_TO_THREAD_POOL ( "loginAction", loginAction );
            loginAction.Start ( );
        }
        else {
            Debug.Log ( "链接不存在" );
        }
    }
    delegate void loginChangeSence();
    public void Click() {
        Button[] pressed = GetComponents<Button> ( );
        string pressedbutton = pressed[0].name;
        Text username = GameObject.Find ( "Background/username/Text" ).GetComponent<Text> ( );
        Text password = GameObject.Find ( "Background/password/Text" ).GetComponent<Text> ( );
        loginController login = (string pressedButton) => {
            switch (pressedButton) {
                case "Login_button":
                    if (username.text != "" && password.text != "") {
                        JSONDATA_Client.LoginData_Normal logindata = new JSONDATA_Client.LoginData_Normal ( );
                        JSONDATA_Client.SendDataJson<JSONDATA_Client.LoginData_Normal> senddata = new JSONDATA_Client.SendDataJson<JSONDATA_Client.LoginData_Normal> ( );
                        logindata.password = password.text;
                        logindata.username = username.text;
                        logindata.action = "login";
                        logindata.type = "normal";
                        senddata.version = StartGame.Config["version"];
                        senddata.server = StartGame.Config["server0"];
                        senddata.data = logindata;

                        LoginAction ( JsonUtility.ToJson(senddata) );
                    }
                    else {
                        Debug.Log ("连接不存在" );
                        GameServer.Start (StartGame.Config["server0"],int.Parse( StartGame.Config["port0"] ) );
                    }
                    break;
                case "qq_login":
                    Debug.Log ( "QQ登录" );

                    break;
                case "wechat_login":
                    Debug.Log ( "微信登录" );
                    break;
                case "register":
                    Debug.Log ( "快速注册" );
                    GameObject client = GameObject.Find ( "ClientNetwork" );
                    if (GameServer.client.Connected) {
                        /*
                         * {"server":"112.74.37.82","version":"1.0.0","data":
                         *      {"action":"login","type":"normal","password":"admin","username":"admin"}
                         *  }
                         */
                        GameServer.ReceiveFlag = false;
                        string str = null;
                        Thread loginAction = new Thread ( () => {
                            string loginInfo = "{\"server\":\"112.74.37.82\",\"version\":\"1.0.0\",\"data\":{\"action\":\"register\",\"type\":\"normal\",\"username\":" + username.text + ",\"password\":'" + password.text + "'}";
                            str = GameServer.SocketSend ( loginInfo );
                            Debug.Log ( str );
                        } );

                        if (!StartGame.ThreadPool.ContainsKey ( "loginAction" )) {
                            Debug.Log ( "添加新线程" );
                            StartGame.ThreadPool.Add ( "loginAction", loginAction );
                        }
                        else {
                            Debug.Log ( "结束老线程" );
                            StartGame.ThreadPool["loginAction"].Abort ( );
                            Debug.Log ( "更改线程" );
                            StartGame.ThreadPool["loginAction"] = loginAction;
                            Debug.Log ( "启动线程" );
                        }
                        loginAction.Start ( );
                    }
                    else {
                        Debug.Log ( "链接不存在" );
                    }
                    break;
                default:
                    Debug.Log ( "错误的数据类型" );
                    break;
            }
        };
        login ( pressedbutton );
    }
    private void Update() {
        if (logSuccess == true) {
            Common.ClearScenceData ( );
            SceneManager.LoadSceneAsync( "Game" );
        }
    }
}
