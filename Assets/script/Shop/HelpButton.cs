﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpButton : MonoBehaviour {
    private GameObject help;
    // Use this for initialization
    private void Start() {
        help = GameObject.Find ( "Canvas/help" );
        help.SetActive (false);
    }
    public void openClick() {
        help.SetActive ( true);
    }
    public void closeClick() {
        help.SetActive (false );
    }
}
