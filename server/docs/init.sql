-- MySQL dump 10.13  Distrib 5.7.18, for Win64 (x86_64)
--
-- Host: localhost    Database: zjhgame
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zjh_session`
--

DROP TABLE IF EXISTS `zjh_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zjh_session` (
  `start_time` bigint(20) DEFAULT NULL COMMENT '登录时间',
  `life` bigint(20) DEFAULT NULL COMMENT 'session生命周期',
  `update_time` bigint(20) DEFAULT NULL COMMENT 'session更新时间',
  `session_hex` varchar(46) DEFAULT NULL COMMENT 'session有效哈希',
  `login_ip` varchar(45) DEFAULT NULL COMMENT '登陆ip地址',
  `uid` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`uid`),
  KEY `fk_zjh_session_zjh_user_idx` (`uid`),
  CONSTRAINT `fk_zjh_session_zjh_user` FOREIGN KEY (`uid`) REFERENCES `zjh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zjh_session`
--

LOCK TABLES `zjh_session` WRITE;
/*!40000 ALTER TABLE `zjh_session` DISABLE KEYS */;
INSERT INTO `zjh_session` VALUES (1501774655228,NULL,NULL,'76TsIWhx9uvQ72k/CaAwOg==','127.0.0.1',3);
/*!40000 ALTER TABLE `zjh_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zjh_user`
--

DROP TABLE IF EXISTS `zjh_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zjh_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id\n',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '用户密码\n',
  `question` varchar(120) DEFAULT NULL COMMENT '安全问题\n',
  `answer` varchar(45) DEFAULT NULL COMMENT '安全问题答案\n',
  `email` varchar(45) DEFAULT NULL COMMENT '用户邮箱\n',
  `telphone` bigint(20) DEFAULT NULL COMMENT '用户电话',
  `face` text COMMENT '用户头像\n',
  `wechat` varchar(45) DEFAULT NULL COMMENT '用户微信',
  `qq_number` int(11) DEFAULT NULL COMMENT '用户QQ号',
  `birthday` date DEFAULT NULL COMMENT '用户生日',
  `alipay` varchar(45) DEFAULT NULL COMMENT '支付宝账号\n',
  `bank_card` bigint(20) DEFAULT NULL COMMENT '银行卡账户\n',
  `realname` varchar(45) DEFAULT NULL COMMENT '用户真实姓名\n',
  `nickname` varchar(45) DEFAULT NULL COMMENT '用户昵称\n',
  `register_time` bigint(20) DEFAULT NULL COMMENT '注册时间',
  `register_ip` varchar(128) DEFAULT NULL COMMENT '注册ip\n',
  `gold` int(11) DEFAULT NULL COMMENT '用户金币数',
  `idcard_number` char(15) DEFAULT NULL COMMENT '用户身份证号\n',
  `address` varchar(128) DEFAULT NULL COMMENT '用户地址\n',
  `level` int(11) DEFAULT '0' COMMENT '用户级别\n',
  `vip_level` int(11) DEFAULT '0' COMMENT 'vip等级\n',
  `need_experience` bigint(20) DEFAULT '1000' COMMENT '升级需要的经验',
  `now_experience` bigint(20) DEFAULT '0' COMMENT '当前经验',
  `vip_need_experience` bigint(20) DEFAULT '0' COMMENT 'vip等级升级需要的经验',
  `vip_now_experience` bigint(20) DEFAULT '0' COMMENT 'vip当前经验值',
  `total_rechage` int(11) DEFAULT '0' COMMENT '总的充值记录数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `uid` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户表\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zjh_user`
--

LOCK TABLES `zjh_user` WRITE;
/*!40000 ALTER TABLE `zjh_user` DISABLE KEYS */;
INSERT INTO `zjh_user` VALUES (3,'admin','admin',NULL,NULL,'admin@qq.com',111203,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin',1501141448358,'127.0.0.1',NULL,NULL,NULL,0,0,1000,0,0,0,0);
/*!40000 ALTER TABLE `zjh_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-04  0:01:04


CREATE TABLE IF NOT EXISTS `zjhgame`.`zjh_friends` (
  `friends_id` INT NULL COMMENT '好友id',
  `add_time` TIME NULL COMMENT '添加时间',
  `present_num` INT NULL COMMENT '这个部分其实计数present表的to_uid即可',
  `uid` INT NOT NULL COMMENT '用户id',
  PRIMARY KEY (`uid`),
  INDEX `fk_zjh_friends_zjh_user1_idx` (`uid` ASC),
  CONSTRAINT `fk_zjh_friends_zjh_user1`
    FOREIGN KEY (`uid`)
    REFERENCES `zjhgame`.`zjh_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '好友列表';