org.xiaojia.beans 数据库表对应的POJO类
org.xiaojia.config 配置文件所在的位置
    apikey.properties
    db.properties         数据库的配置文件
    mybatis.xml           mybatis配置事务管理和数据库连接池配置
    redis.properties      redis分布式配置
    system.properties     系统配置
org.xiaojia.config.mapper mapper映射对应文件存放位置
    BoardMapper.xml       公告表映射文件
    SessionMapper.xml     session表的映射
    UserMapper.xml        用户表的映射
org.xiaojia.dao           数据访问层接口
org.xiaojia.dao.impl      数据访问层实现
org.xiaojia.log           log操作类
org.xiaojia.main          主程序入口
org.xiaojia.redis         操作redis类
org.xiaojia.rsa           rsa加密工具包
org.xiaojia.service       service层实现
org.xiaojia.test          Junit使用的测试类，主要是进行单元测试
org.xiaojia.utils         工具类
    CommandTransfer       发送数据的工具类
    DababaseUtil          数据库操作类
    Md5Util               MD5编码类
    RedisUtil
log4j.properties          log4j的配置文件
pom.xml                   maven的依赖管理文件
pri.key                   私钥
pub.key                   公钥