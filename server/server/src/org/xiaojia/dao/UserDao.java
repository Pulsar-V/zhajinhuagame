package org.xiaojia.dao;


import java.util.List;

import org.xiaojia.beans.Session;
import org.xiaojia.beans.User;

/**
 * 操作用户表的接口
 * @author XiaoJia
 *
 */
public interface UserDao {	
	
	public List<User> getAllList();
	
	public int update(int id);
	
	public User getUser(User user, Session session);
	
	public User getUser(int id);
	
	public int addUser(User user);
	
}
