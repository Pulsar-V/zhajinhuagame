package org.xiaojia.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.xiaojia.beans.Session;
import org.xiaojia.beans.User;
import org.xiaojia.dao.UserDao;
import org.xiaojia.utils.DatabaseUtil;


/**
 * 操作用户表的Dao实现类
 * @author XiaoJia
 *
 */
public class UserDaoImpl implements UserDao {

	@Override
	public List<User> getAllList() {
		
		return null;
	}

	@Override
	public int update(int id) {
		return 0;
	}

	@Override
	public User getUser(User loginUser,Session session) {
		SqlSession sqlSession = null;
		User user = null;
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession(false);
			user=sqlSession.selectOne("User.queryUserByUser",loginUser); 
			session.setUid(user.getId());
			sqlSession.insert("Session.insertSession",session);
			sqlSession.commit();
		}catch(Exception e){
			sqlSession.rollback();
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
		return user;
	}

	@Override
	public User getUser(int id) {
		SqlSession sqlSession = null;
		User user = null;
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			user=sqlSession.selectOne("Friend.insert",id); 
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
		return user;
	}

	@Override
	public int addUser(User user) {
		SqlSession sqlSession = null;
		int ret = 0;
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			ret= sqlSession.insert("User.insertUser",user);
			sqlSession.commit();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
		return ret;
	}

}
