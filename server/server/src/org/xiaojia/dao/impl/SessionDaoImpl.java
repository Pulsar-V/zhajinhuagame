package org.xiaojia.dao.impl;

import org.apache.ibatis.session.SqlSession;
import org.xiaojia.beans.Session;
import org.xiaojia.dao.SessionDao;
import org.xiaojia.log.LogerBuild;
import org.xiaojia.utils.DatabaseUtil;

/**
 * 操作Session的实现类
 * @author XiaoJia
 *
 */
public class SessionDaoImpl implements SessionDao {
	
	public Session getSession(String sessionHash){
		SqlSession sqlSession = null;
		Session session = null;
		
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			session=sqlSession.selectOne("Session.querySessionBySessionHash",sessionHash); 
		}catch(Exception e){
			LogerBuild.Error(e.getMessage());
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
		return session;
	}
}
