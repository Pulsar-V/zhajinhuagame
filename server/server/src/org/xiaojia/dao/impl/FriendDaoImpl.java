package org.xiaojia.dao.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.xiaojia.beans.Friend;
import org.xiaojia.dao.FriendDao;
import org.xiaojia.utils.DatabaseUtil;

/**
 * 好友列表操作实现类
 * @author XiaoJia
 *
 */
public class FriendDaoImpl implements FriendDao {

	@Override
	public int addFriend(Friend friend) {
		SqlSession sqlSession = null;
		int ret = 0;
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			ret= sqlSession.insert("Friend.insertUser",friend);
			sqlSession.commit();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
		return ret;
	}

	/**
	 * 			列表中的每一个元素
	 *  		"name":"xxxx", //nickname  
	            "face":"http://xxxxxxx.jpg",//0为锁定1为开放
	            "state":"1",//1代表在线0代表离线:是否在线可以查看session是否存在
	            "level":"1" //用户等级
	 */
	@Override
	public List<Friend> getFriend(int uid,int offset, int limit) {
		List<Friend> list = null;
		SqlSession sqlSession = null;
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			//这里返回的是用户id为uid的信息
			list=sqlSession.selectList("Friend.queryByUid",uid,new RowBounds(offset,limit)); 
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
		return list;
	}
	
}
