package org.xiaojia.dao;


import java.util.List;

import org.xiaojia.beans.Friend;

/**
 * 操作好友表的接口
 * @author XiaoJia
 * 
 */
public interface FriendDao {

	/**
	 * 添加好友对象
	 * @param friend
	 * @return
	 */
	int addFriend(Friend friend);

	/**
	 * 根据用户id获取好友列表
	 * @param uid
	 * @return
	 */
	List<Friend> getFriend(int uid,int offset, int limit);

}
