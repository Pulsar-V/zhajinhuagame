package org.xiaojia.dao;

import org.xiaojia.beans.Session;

/**
 * 操作用户Session表的接口
 * @author XiaoJia
 *
 */
public interface SessionDao {
	
	public Session getSession(String sessionHash);
}
