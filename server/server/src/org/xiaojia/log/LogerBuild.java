package org.xiaojia.log;

import org.apache.log4j.Logger;

/**
 * Created by Pulsar-V on 2017/7/29.
 * Modify by XiaoJia on 2017/7/29.使用log4j来操作日志
 */
public class LogerBuild {
	private static Logger logger = Logger.getLogger(LogerBuild.class.getName());
    
	public static void Error(String info){
    	logger.error(info); 
    }
    
    public static void Success(String info){
    	logger.debug(info);
    }
    
    public static void INFO(String info){
    	logger.info(info);
    }
    
    
    public static void fatal(String info){
    	logger.fatal(info);
    }
    
}
