package org.xiaojia.service;

import java.util.List;

import org.xiaojia.beans.Friend;
import org.xiaojia.dao.FriendDao;
import org.xiaojia.dao.impl.FriendDaoImpl;

/**
 * 处理好友请求的service
 * @author XiaoJia
 */
public class FriendsService {
	private FriendDao friendsDao;
	
	public FriendsService() {
		friendsDao=new FriendDaoImpl();
	}
	
	public FriendsService(FriendDao friendsDao){
		this.friendsDao = friendsDao;
	}
	
	/**
	 * 处理添加好友请求
	 * @param friend
	 * @return
	 */
	public boolean addFriend(Friend friend) {
		int id = friendsDao.addFriend(friend);
		if(id>0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 根据用户编号来获取好友
	 * @param uid
	 * @return
	 */
	public List<Friend> getFriend(int uid,int offset, int limit){
		return friendsDao.getFriend(uid,offset,limit);
	}

}
