package org.xiaojia.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xiaojia.beans.Friend;
import org.xiaojia.beans.Room;
import org.xiaojia.beans.Session;
import org.xiaojia.beans.User;
import org.xiaojia.rsa.RSA;
import org.xiaojia.utils.Md5Util;



public class SocketThread implements Runnable {
	/**
	 * 用于获取socket对象，获取用户连接IP地址
	 */
	private Socket socket = null;
	/**
	 * 用户接收用户输入
	 */
	private BufferedReader bufferedReader = null;
	/**
	 * 用于向客户端发送json数据
	 */
	private BufferedWriter bufferedWriter = null;

	private RSA rsaServer = null;
	private UserService userService = new UserService();
	private RoomService roomService = new RoomService();
	private SessionService sessionService = new SessionService();
	private FriendsService friendsService = new FriendsService();
	/**
	 * 用于判断线程是否停止
	 */
	private volatile boolean exit = false;
	/**
	 * 线程安全的计数变量
	 */
	private static AtomicInteger count = new AtomicInteger(1);

	public SocketThread() {
		rsaServer = new RSA();
		try {
			userService = new UserService();
			roomService = new RoomService();
			sessionService = new SessionService();
			friendsService = new FriendsService();
			exit = false;
			count = new AtomicInteger(1);
			rsaServer.loadPrivateKeyFile(("priv.key"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SocketThread(Socket socket) {
		this();
		this.socket = socket;
	}

	@Override
	public synchronized void run() {
		while (!exit) {
			count.incrementAndGet();// 自增1,返回更新值
			InputStream is = null;
			try {
				is = socket.getInputStream();
				byte[] buf = new byte[1];
				StringBuffer stringBuffer = new StringBuffer();
				while (is.read(buf) != -1) {
					if (buf[0] == "#".getBytes()[0]) {
						break;
					}
					stringBuffer.append(new String(buf));
				}
				// 为了防止构造json异常，和空指针异常
				System.out.println(stringBuffer.toString());
				
				JSONObject object = null;
				JSONObject result = null;
				
				if (stringBuffer.length() != 0) {
					object = new JSONObject(stringBuffer.toString());
					result = excute(object);
				}
				
				// 返回结果集给客户端
				if (result != null) {
					bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
					bufferedWriter.write(result.toString() + "#");
					bufferedWriter.flush();
				}
				
				//判断线程是否结束
				stop();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 执行客户端指令操作
	 * 
	 * @param JSONObjet
	 * @return JSONObject
	 */
	private JSONObject excute(JSONObject object) {
		// 对接收到的json对象进行解密操作
		Map<String, Object> map = decrypt(object);
		String cmd = (String) map.get("cmd");
		InetSocketAddress socketAddress = (InetSocketAddress) (this.socket.getRemoteSocketAddress());
		String lastIp = socketAddress.getHostString();
		Map<String, Object> result = new HashMap<String, Object>();
		if (cmd.equals("register")) {// 用户注册
			User user = new User();
			user.setRegisterIp(lastIp);
			user.setRegisterTime(System.currentTimeMillis());
			user.setUsername((String) map.get("username"));
			user.setPassword((String) map.get("password"));
			user.setNickname((String) map.get("nickname"));
			user.setTelphone(Long.parseLong((String) map.get("telphone")));
			user.setEmail((String) map.get("telphone"));
			boolean flag = userService.addUser(user);
			if (flag) {
				result.put("state", "success");
			} else {
				result.put("state", "failed");
			}
		} else if (cmd.equals("login")) {// 用户登录
			// 接收用户对象
			User user = new User();
			user.setLoginType((String) map.get("type"));
			user.setUsername((String) map.get("username"));
			user.setPassword((String) map.get("password"));
			// 存储session
			Session session = new Session();
			long startTime = System.currentTimeMillis();
			session.setStartTime(startTime); // 登录时间
			session.setLoginIp(lastIp); // 登录ip
			String sessionHex = Md5Util.md5(startTime + user.getUsername() + socket.getLocalAddress().getHostAddress());
			session.setSessionHex(sessionHex); // session哈希

			// 返回数据给客户端
			if ("normal".equals(user.getLoginType())) {
				user = userService.loginUser(user, session);
				boolean flag = user != null ? true : false;
				if (flag) {
					result.put("server", socket.getLocalAddress().getHostAddress());
					result.put("version", "1.0.0");
					result.put("state", "success");

					HashMap<String, Object> sessionMap = new HashMap<String, Object>();
					sessionMap.put("user_id", user.getId());
					sessionMap.put("login_time", session.getStartTime());
					sessionMap.put("session_hash", session.getSessionHex());
					result.put("session", sessionMap);
				} else {
					result.put("state", "failed");
				}
			} else if ("qq".equals(user.getLoginType())) {
				result.put("ret", "0");
				result.put("pay_token", "");
				result.put("pf", "openmobile_android");
				result.put("expires_in", "7776000");
				result.put("pfkey", "");
				result.put("access_token", "");
				result.put("msg", "success");
				// TODO:微信的英文名叫wechat
			} else if ("wechat".equals(user.getLoginType())) {

			} else {

			}
		} else if ("createRoom".equals(cmd)) {
			// 接收数据
			Room room = new Room();
			// 设置房间索引
			String key = "room" + room.getType() + room.hashCode();
			// 设置房间id
			room.setId("" + room.hashCode());
			boolean isOk = this.roomService.SetRoom(key, room);
			// 判断并返回数据
			if (isOk) {
				result.put("id", room.getId());
				result.put("masterUid", room.getMasterUid());
				result.put("password", room.getPassword());
				result.put("type", room.getType());
				result.put("uid", room.getUid());
				result.put("state", "success");
			} else {
				result.put("state", "failed");
			}
		} else if ("addRoom".equals(cmd)) {
			// 设置获取传入的参数
			String userId = (String) map.get("id");
			String key = "room" + map.get("roomId");
			String password = (String) map.get("password");
			// 写入redis
			result = this.roomService.addRoom(key, password, userId);
		} else if ("dismissRoom".equals(cmd)) {
			// 获取传入的参数
			String key = "room" + map.get("roomId");
			RoomService roomService = new RoomService();
			result = roomService.dismissRoom(key);
		} else if ("exitRoom".equals(cmd)) {

		} else if ("getuserinfo".equals(cmd)) {
			// 接收数据
			int uid = Integer.parseInt((String) map.get("uid"));
			String sessionHash = (String) map.get("sessionhash");
			// 校验sessionhash
			Session session = this.sessionService.getSession(sessionHash);
			if (session != null) {
				// 获取用户信息
				User user = this.userService.getUser(uid);
				// 返回数据
				result.put("face", user.getFace());
				result.put("nickname", user.getNickname());
				result.put("level", user.getLevel());
				result.put("gold", user.getGold());
				result.put("vip_level", user.getVipLevel());
				result.put("idcard_number", user.getIdcardNUmber());
				result.put("wechat", user.getWechat());
				result.put("telphone", user.getTelphone());
				result.put("bank_card", user.getBankCard());
				result.put("vip_level", user.getVipLevel());
				result.put("need_experience", user.getNeedExperience());
				result.put("now_experience", user.getNowExperience());
				result.put("qq_number", user.getQqNumber());
				result.put("email", user.getEmail());
			} else {
				result.put("state", "failed");
			}
		} else if ("enterhell".equals(cmd)) {
			// 接收数据
			int uid = Integer.parseInt((String) map.get("uid"));
			String sessionHash = (String) map.get("sessionhash");
			String type = (String) map.get("type");
			// 校验sessionhash
			Session session = sessionService.getSession(sessionHash);
			if (uid != session.getUid()) {
			}
			Set<String> set = null;
			if ("primary".equals(type)) {
				set = roomService.getAllRoom("room" + type + "*");
			} else if ("intermediate".equals(type)) {
				set = roomService.getAllRoom("room" + type + "*");
			} else if ("high".equals(type)) {
				set = roomService.getAllRoom("room" + type + "*");
			} else {
				result.put("state", "failed");
			}
			// 遍历set，然后存入list中
			if (set != null) {
				List<Map<String, String>> list = new ArrayList<Map<String, String>>();
				for (String string : set) {
					list.add(roomService.getRoom(string));
				}
				result.put("time", System.currentTimeMillis());
				result.put("state", "success");
				result.put("room", list);
			}

		} else if ("getfriends".equals(cmd)) {
			int uid = Integer.parseInt((String) map.get("uid"));
			String sessionHash = (String) map.get("sessionhash");
			int offset =  (int) map.get("offset");
			int limit =  (int) map.get("limit");
			// 校验sessionhash
			Session session = sessionService.getSession(sessionHash);
			if (uid == session.getUid()) {
				List<Friend> list = friendsService.getFriend(uid,offset,limit);
				// 返回数据
				result.put("time", System.currentTimeMillis());
				result.put("state", "success");
				result.put("friends", list);
			} else {
				result.put("state", "failed");
			}
		} else if("gettasklist".equals(cmd)){
			//获取数据
			int uid = Integer.parseInt((String) map.get("uid"));
			String sessionHash = (String) map.get("sessionhash");
			int offset =  (int) map.get("offset");
			int limit =  (int) map.get("limit");
			// 校验sessionhash
			Session session = sessionService.getSession(sessionHash);
			if (uid == session.getUid()) {
				List<Friend> list = friendsService.getFriend(uid,offset,limit);
				// 返回数据
				result.put("time", System.currentTimeMillis());
				result.put("state", "success");
				result.put("task", list);
			} else {
				result.put("state", "failed");
			}

		}else if("getshopinfo".equals(cmd)){
			//获取数据
			int uid = Integer.parseInt((String) map.get("uid"));
			String sessionHash = (String) map.get("sessionhash");
			int offset =  (int) map.get("offset");
			int limit =  (int) map.get("limit");
			
			// 校验sessionhash
			Session session = sessionService.getSession(sessionHash);
			if (uid == session.getUid()) {
				List<Friend> list = friendsService.getFriend(uid,offset,limit);
				// 返回数据
				result.put("time", System.currentTimeMillis());
				result.put("state", "success");
				result.put("data", list);
			} else {
				result.put("state", "failed");
			}
		}
		return new JSONObject(result);
	}

	/**
	 * 对接收的json对象进行解密
	 * 
	 * @param jsonObject
	 */
	private Map<String, Object> decrypt(JSONObject jsonObject) {
		Iterator<String> iterator = jsonObject.keys();
		Map<String, Object> result = new HashMap<String, Object>();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			// 有可能值也是一个json
			Object object = jsonObject.get(key);
			if (object instanceof JSONObject) {
				JSONObject temp = (JSONObject) object;
				Map<String, String> value = new HashMap<String, String>();
				Iterator<String> iteratorTemp = temp.keys();
				while (iterator.hasNext()) {
					String tempKey = (String) iteratorTemp.next();
					String tempValue = rsaServer.decryptData((String) temp.get(tempKey));
					value.put(tempKey, tempValue);
				}
				result.put(key, value);
			} else {
				String value1 =(String) jsonObject.get(key);
				String value = rsaServer.decryptData(value1);
				result.put(key, value);
			}

		}
		return result;
	}

	/**
	 * 线程停止,设置标志位，线程计数器减一，关闭相应资源
	 * 
	 * @throws IOException
	 */
	private void stop() throws IOException {
			exit = true;
			count.decrementAndGet();
			close(bufferedReader, bufferedWriter, socket);
	}

	/**
	 * 负责关闭所有可关闭资源
	 * 
	 * @param closeables
	 * @throws IOException
	 */
	private void close(Closeable... closeables) throws IOException {
		for (Closeable closeable : closeables) {
			if (closeable != null) {
				closeable.close();
			}
		}
	}

	/**
	 * 获取线程计数器
	 * 
	 * @return
	 */
	public static AtomicInteger getCount() {
		return count;
	}
}
