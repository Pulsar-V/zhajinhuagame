package org.xiaojia.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.xiaojia.beans.Room;
import org.xiaojia.redis.RoomDao;

/**
 * 房间请求的处理类
 * 
 * @author XiaoJia
 *
 */
public class RoomService {
	public RoomDao roomDao;

	public RoomService() {
		roomDao = new RoomDao();
	}

	public RoomService(RoomDao roomDao) {
		this.roomDao = roomDao;
	}

	/**
	 * 创建房间
	 * @param key
	 * @param room
	 */
	public boolean SetRoom(String key, Room room) {
		if ("primary".equals(room.getType())) {
			 return roomDao.setRoom(key, room);
		} else if ("intermediate".equals(room.getType())) {
			return roomDao.setRoom(key, room);
		} else if ("high".equals(room.getType())) {
			return roomDao.setRoom(key, room);
		} else {
			return false;
		}
	}

	/**
	 * 添加房间,
	 * 
	 * @param key
	 * @param password
	 * @param userId
	 * @return Map<String, Object> 返回错误提示信息和状态码
	 */
	public Map<String, Object> addRoom(String key, String password, String userId) {
		return roomDao.addRoom(key, password, userId);
	}

	/**
	 * 解散房间
	 * 
	 * @param key
	 */
	public Map<String, Object> dismissRoom(String key) {
		 Map<String, Object> result = new HashMap<String, Object>();
		if(roomDao.destroyRoom(key)==1){
			result.put("message", "解散成功");
			result.put("state", "success");
		}else{
			result.put("message", "解散失败");
			result.put("state","failed");
		}
		return result;
		
	}

	/**
	 * 获取所有房间的key，存放在set里面
	 * @param key
	 * @return
	 */
	public Set<String> getAllRoom(String key) {
		return roomDao.GetRoomsBykey(key);
	}
	
	public Map<String, String> getRoom(String key) {
		return roomDao.GetRoom(key);
	}
}
