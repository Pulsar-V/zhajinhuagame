package org.xiaojia.service;

import org.xiaojia.beans.Session;
import org.xiaojia.beans.User;
import org.xiaojia.dao.UserDao;
import org.xiaojia.dao.impl.UserDaoImpl;

/**
 * 用户服务，用于处理用户请求
 * @author XiaoJia
 *
 */
public class UserService {
	private UserDao userDao;
	
	public UserService() {
		userDao=new UserDaoImpl();
	}
	
	public UserService(UserDao userDao){
		this.userDao = userDao;
	}
	
	/**
	 * 处理用户注册请求
	 * @param user
	 * @return
	 */
	public boolean addUser(User user) {
		int id = userDao.addUser(user);
		if(id>0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 处理用户登录请求,登录成功并记录最后登录时间和登录IP
	 * @param user对象
	 * @return 返回是否登录成功
	 */
	public User loginUser(User user,Session session) {
		return userDao.getUser(user,session);
	}
	
	/**
	 * 根据用户编号来获取用户
	 * @param id
	 * @return
	 */
	public User getUser(int id){
		return userDao.getUser(id);
	}

}
