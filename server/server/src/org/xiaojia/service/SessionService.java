package org.xiaojia.service;

import org.xiaojia.beans.Session;
import org.xiaojia.dao.SessionDao;
import org.xiaojia.dao.impl.SessionDaoImpl;

/**
 * Session服务
 * @author XiaoJia
 *
 */
public class SessionService {
	private SessionDao sessionDao;
	
	public SessionService() {
		sessionDao=new SessionDaoImpl();
	}
	
	public SessionService(SessionDao userDao){
		this.sessionDao = userDao;
	}
	
	public Session getSession(String sessionHash){
		return sessionDao.getSession(sessionHash);
	}
	
}
