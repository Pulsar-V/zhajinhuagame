package org.xiaojia.service;

import org.xiaojia.log.LogerBuild;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

/**
 * 创建主服务的类
 * @author XiaoJia
 *
 */
public class SocketServer {
<<<<<<< HEAD
=======

>>>>>>> bb1b749f572bd3d693e5d3bd83e80aa4b4d24f87
	public void start() {
		ServerSocket serverSocket = null;
		InputStream in = ClassLoader.getSystemResourceAsStream("org/xiaojia/config/system.properties");
		Properties prop = new Properties();
		try {
			prop.load(in);
			serverSocket = new ServerSocket();
			String hostname= prop.getProperty("ip");
			int port= Integer.parseInt(prop.getProperty("port"));
			serverSocket.bind(new InetSocketAddress(hostname, port));
<<<<<<< HEAD
			LogerBuild.Success("服务器端"+hostname+"在端口"+port+"启动，等待客户连接");
=======
			LogerBuild.Success("服务器端+"+hostname+"在端口"+port+"启动，等待客户连接");
>>>>>>> bb1b749f572bd3d693e5d3bd83e80aa4b4d24f87
			Socket socket = null;
			while (true) {
				socket = serverSocket.accept();
				SocketThread st = new SocketThread(socket);
				Thread thread = new Thread(st);
				thread.start();
				LogerBuild.INFO("当前连接的客户端数量为：" + SocketThread.getCount());
			}
		} catch (Exception e) {
			LogerBuild.Error(e.getMessage());
		} finally {
			try {
				serverSocket.close();
				LogerBuild.Error("服务器关闭");
			} catch (IOException e) {
				LogerBuild.Error(e.getMessage());
			}
		}
	}
}
