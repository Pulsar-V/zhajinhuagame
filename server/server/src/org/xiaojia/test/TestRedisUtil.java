package org.xiaojia.test;

import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;
import org.xiaojia.beans.Room;
import org.xiaojia.redis.RoomDao;
import org.xiaojia.utils.RedisUtil;

import redis.clients.jedis.ShardedJedis;

/**
 * 测试redis
 * @author XiaoJia
 */
public class TestRedisUtil {

	@Test
	public void test() {

		RoomDao roomDao = new RoomDao();
		Room room = new Room("1,2,3,4,5,6", false, "", "123", "primary", "1", "1", "5");
		Room room2 =new Room("1,2,3,4,5,6", false, "", "123", "primary", "1", "1", "5");

		System.out.println(room.hashCode());
		System.out.println(room2.hashCode());
		roomDao.setRoom("room"+room.getType()+room.hashCode(), room);
		roomDao.setRoom("room"+room2.getType()+room2.hashCode(), room2);
		// roomDao.destroyRoom("key");

		ShardedJedis jedis = RedisUtil.getInstance().getSharedJedis();
		String result = jedis.hget("room1611070867", "uid");
		System.out.println(result);
	}

	@Test
	public void test2() {
		RoomDao roomDao = new RoomDao();
		Map<String, String> map = roomDao.GetRoom("key1");
		System.out.println(map);
		for (Entry<String, String> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}
	}
}
