package org.xiaojia.test;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.xiaojia.beans.User;
import org.xiaojia.utils.DatabaseUtil;

/*
 * 测试相关的dao类
 */

public class TestUserDao {

	@Test()
	public void TestQueryUserById() {
		SqlSession sqlSession = null;
		User user = null;
		try {
			// new DBCPDataSourceFactory();
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			user = sqlSession.selectOne("User.queryUserById", 1);
			if (user != null) {
				System.out.println(user.getId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}
	}

}
