package org.xiaojia.test;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.xiaojia.beans.Session;
import org.xiaojia.service.SessionService;
import org.xiaojia.utils.DatabaseUtil;
/**
 * 
 * @author XiaoJia
 *
 */
public class TestSessionDao {
	
	@Test
	public void test(){
		SqlSession sqlSession = null;
		try{
			DatabaseUtil databaseUtils = DatabaseUtil.getInstance();
			sqlSession = databaseUtils.getSession();
			Session session = new Session(111, 111, 222, "111", "223211", 3);
			sqlSession.insert("Session.insertSession",session);
			sqlSession.commit();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
	}
	
	
	@Test
	public void testSessionService(){
		SessionService service = new SessionService();
		Session session=service.getSession("223211");
		System.out.println(session.getSessionHex());
	}
	
}
