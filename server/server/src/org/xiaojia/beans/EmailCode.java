package org.xiaojia.beans;

import org.jetbrains.annotations.Contract;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Transport;
import java.util.Date;

/**
 * 邮箱操作类
 * @author Pulsar-V
 */
@SuppressWarnings("unused")
public class EmailCode implements Serializable {

    private static final long serialVersionUID = 1L;

    private int CODELEN;
    private String EmailAccount = "xxxxxxxxx@163.com";
    private String EmailPassword = "xxxxxxxxx";
    private String EmailSMTPHost = "smtp.163.com";

    EmailCode() {
        Properties prop = new Properties();
        try {
            InputStream is = new FileInputStream("org/xiaojia/config/apikey.properties");
            prop.load(is);
            this.CODELEN = Integer.parseInt(prop.getProperty("CODE_LENGTH"));
            this.EmailAccount = prop.getProperty("EmailAccount");
            this.EmailPassword = prop.getProperty("EmailPassword");
            this.EmailSMTPHost = prop.getProperty("EmailSMTPHost");
        } catch (Exception e) {
            //TODO:log eror
        }

    }

    public boolean sendCode(String receiveMailAccount) {
        try {
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");
            props.setProperty("mail.smtp.host", this.EmailSMTPHost);
            props.setProperty("mail.smtp.auth", "true");
            Session session = Session.getDefaultInstance(props);
            session.setDebug(true);
            MimeMessage message = this.createMimeMessage(session, this.EmailAccount, receiveMailAccount);
            if (message != null) {
                Transport transport = session.getTransport();
                transport.connect(this.EmailAccount, this.EmailPassword);
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    @Contract(pure = true)
    private MimeMessage createMimeMessage(Session session, String emailaccount, String receivemailaccount) {
        try {
            MimeMessage message = new MimeMessage(session);     // 创建邮件对象
            // 2. From: 发件人
            //    其中 InternetAddress 的三个参数分别为: 邮箱, 显示的昵称(只用于显示, 没有特别的要求), 昵称的字符集编码
            //    真正要发送时, 邮箱必须是真实有效的邮箱。
            message.setFrom(new InternetAddress(this.EmailAccount, "USER_AA", "UTF-8"));

            // 3. To: 收件人
            message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress("cc@receive.com", "USER_CC", "UTF-8"));
            //    To: 增加收件人（可选）
            message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress("dd@receive.com", "USER_DD", "UTF-8"));
            //    Cc: 抄送（可选）
            message.setRecipient(MimeMessage.RecipientType.CC, new InternetAddress("ee@receive.com", "USER_EE", "UTF-8"));
            //    Bcc: 密送（可选）
            message.setRecipient(MimeMessage.RecipientType.BCC, new InternetAddress("ff@receive.com", "USER_FF", "UTF-8"));

            // 4. Subject: 邮件主题
            message.setSubject("TEST邮件主题", "UTF-8");

            // 5. Content: 邮件正文（可以使用html标签）
            message.setContent("TEST这是邮件正文。。。", "text/html;charset=UTF-8");

            // 6. 设置显示的发件时间
            message.setSentDate(new Date());

            // 7. 保存前面的设置
            message.saveChanges();
            return message;
        } catch (Exception e) {
            //TODO:get error
        }
        return null;
    }
}
