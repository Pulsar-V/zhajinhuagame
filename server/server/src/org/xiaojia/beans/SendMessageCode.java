package org.xiaojia.beans;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xiaojia.log.LogerBuild;

/**
 *
 *短信操作类
 * @author Norse
 *
 */
@SuppressWarnings("unused")
public class SendMessageCode {
	private   String SERVER_URL;
	private   String APP_KEY;
	private   String APP_SECRET;
	private   String NONCE = "123456";
	private   String TEMPLATEID = "3057527";
	private   String MOBILE;
	private   String CODELEN ;
	SendMessageCode(String MOBILE){
		Properties prop = new Properties();
		try{
			InputStream is = new FileInputStream("apikey.properties");
			prop.load(is);
		    this.SERVER_URL=prop.getProperty("NETEASE_SERVER_URL");
		    this.APP_SECRET=prop.getProperty("NETEASE_APP_SECRET");
		    this.TEMPLATEID=prop.getProperty("NETEASE_TEMPLATEID");
		    this.CODELEN=prop.getProperty("CODE_LENGTH");
		    this.APP_KEY=prop.getProperty("NETEASE_APP_KEY");
		    this.MOBILE=MOBILE;
		}catch (Exception e){
			LogerBuild.fatal(e.getMessage());
		}
	}
	public  void Send(String telphone) throws Exception {
		//DefaultHttpClient httpClient = new DefaultHttpClient();

		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
		HttpPost httpPost = new HttpPost(SERVER_URL);
		String curTime = String.valueOf((new Date()).getTime() / 1000L);
		String checkSum = CheckSum.getCheckSum(APP_SECRET, NONCE, curTime);

		httpPost.addHeader("AppKey", APP_KEY);
		httpPost.addHeader("Nonce", NONCE);
		httpPost.addHeader("CurTime", curTime);
		httpPost.addHeader("CheckSum", checkSum);
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		nvps.add(new BasicNameValuePair("templateid", TEMPLATEID));
		nvps.add(new BasicNameValuePair("mobile", telphone));
		nvps.add(new BasicNameValuePair("codeLen", CODELEN));

		httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

		HttpResponse response = closeableHttpClient.execute(httpPost);

		System.out.println(EntityUtils.toString(response.getEntity(), "utf-8"));
		
		//关闭资源
		if (closeableHttpClient!= null) {
			closeableHttpClient.close();
		}
	}
}
