package org.xiaojia.beans;

/**
 * 用户存放用户Session的实体类
 * @author XiaoJia
 *
 */
public class Session{
	private long startTime;
	/**
	 * Session生命周期
	 */
	private long life;
	/**
	 * Session更新时间
	 */
	private long updateTime;
	/**
	 * 登录IP地址
	 */
	private String loginIp;
	/**
	 * session有效哈希
	 */
	private String sessionHex;
	
	private int uid;
	
	
	public Session(){}
	
	public Session(long startTime, long life, long updateTime, String loginIp, String sessionHex, int uid) {
		super();
		this.startTime = startTime;
		this.life = life;
		this.updateTime = updateTime;
		this.loginIp = loginIp;
		this.sessionHex = sessionHex;
		this.uid = uid;
	}

	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getLife() {
		return life;
	}
	public void setLife(long life) {
		this.life = life;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	public String getSessionHex() {
		return sessionHex;
	}
	public void setSessionHex(String sessionHex) {
		this.sessionHex = sessionHex;
	}
	
}
