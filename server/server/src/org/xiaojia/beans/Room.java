package org.xiaojia.beans;

/**
 * 用于存放房间对象
 * @author XiaoJia
 *
 */
public class Room {
	/**
	 * 存放用户id的字符串
	 */
	private String uid;
	/**
	 * 是否加密
	 */
	private boolean lock;
	/**
	 * 房间密码
	 */
	private String password;
	/**
	 * 庄家
	 */
	private String banker;
	/**
	 * 房间类型
	 */
	private String type;
	/**
	 * 房间id
	 */
	private String id;
	/**
	 * 房主用户id
	 */
	private String masterUid;
	/**
	 * 房间人数
	 */
	private String state;

	public Room() {
	}

	public Room(String uid, boolean lock, String password, String banker, String type, String id, String masterUid,String state) {
		super();
		this.uid = uid;
		this.lock = lock;
		this.password = password;
		this.banker = banker;
		this.type = type;
		this.id = id;
		this.masterUid = masterUid;
		this.state= state;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public boolean isLock() {
		return lock;
	}

	public void setLock(boolean lock) {
		this.lock = lock;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBanker() {
		return banker;
	}

	public void setBanker(String banker) {
		this.banker = banker;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMasterUid() {
		return masterUid;
	}

	public void setMasterUid(String masterUid) {
		this.masterUid = masterUid;
	}

	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
