package org.xiaojia.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pulsar-V on 2017/7/31.
 *
 * 构造实时游戏状态
 */
public class GameTable {
    /**
     * 构造空白单项矩阵
     * @return
     */
    private int[][]createEmptySingalMartix(){
        return new int[][]{
                {0},{0},{0},{0},
                {0},{0},{0},{0},
                {0},{0},{0},{0},
                {0},{0},{0},{0}
        };
    }
}
