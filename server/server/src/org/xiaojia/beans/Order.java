package org.xiaojia.beans;

/**
 * 订单表
 * @author XiaoJia
 *
 */
public class Order {
	private int id;
	private int userId;
	private String  orderNumer;
	private String pay_way;
	private boolean is_pay;
	private String shop_name;
	private int shopId;
	
	public Order(){
		
	}
	
	public Order(int id, int userId, String orderNumer, String pay_way, boolean is_pay, String shop_name, int shopId) {
		this.id = id;
		this.userId = userId;
		this.orderNumer = orderNumer;
		this.pay_way = pay_way;
		this.is_pay = is_pay;
		this.shop_name = shop_name;
		this.shopId = shopId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getOrderNumer() {
		return orderNumer;
	}
	public void setOrderNumer(String orderNumer) {
		this.orderNumer = orderNumer;
	}
	public String getPay_way() {
		return pay_way;
	}
	public void setPay_way(String pay_way) {
		this.pay_way = pay_way;
	}
	public boolean isIs_pay() {
		return is_pay;
	}
	public void setIs_pay(boolean is_pay) {
		this.is_pay = is_pay;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	
}
