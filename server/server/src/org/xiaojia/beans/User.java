package org.xiaojia.beans;

import java.util.Date;

/**
 * 用户用户表
 * @author XiaoJia
 *
 */
public class User  {
	/**
	 * 用户编号
	 */
	private int id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 用户密码
	 */
	private String password;
	/**
	 * 用户问题
	 */
	private String question;
	/**
	 * 用户答案
	 */
	private String answer;
	/**
	 * 用户邮箱
	 */
	private String email;
	/**
	 * 用户电话
	 */
	private long telphone;
	/**
	 * 用户头像
	 */
	private String face;
	/**
	 * 用户微信号
	 */
	private String wechat;
	/**
	 * 用户QQ号
	 */
	private String qqNumber;
	/**
	 * 用户生日
	 */
	private Date birthday;
	/**
	 * 用户支付宝账号
	 */
	private String alipay;
	/**
	 * 用户银行卡号
	 */
	private String bankCard;
	/**
	 * 用户真实姓名
	 */
	private String realname;
	/**
	 * 用户昵称
	 */
	private String nickname;
	/**
	 * 用户注册时间
	 */
	private long registerTime;
	/**
	 * 用户注册ip
	 */
	private String registerIp;
	/**
	 * 用户金币数
	 */
	private long gold;
	/**
	 * 用户省份证号
	 */
	private String idcardNUmber;
	/**
	 * 用户地址
	 */
	private String address;
	/**
	 * 用户等级
	 */
	private long   level;
	/**
	 * 用户Vip等级
	 */
	private long   vipLevel;
	/**
	 * 用户等级升级所需要的经验
	 */
	private long needExperience;
	/**
	 * 用户当前经验值
	 */
	private long nowExperience;
	/**
	 * 用户vip等级升级所需要的经验
	 */
	private long vipNeedExperience;
	/**
	 * 用户vip当前经验值
	 */
	private long vipNowExperience;
	/**
	 * 用户当前充值数
	 */
	private int total_rechage;
	/**
	 * 登录类型
	 */
	private String loginType;

	public User(){
		
	}

	public User(int id, String username, String password, String question, String answer, String email, long telphone,
			String face, String vchate, String qqNumber, Date birthday, String alipay, String bankCard, String realname,
			String nickname, long registerTime, String registerIp, long gold, String idcardNUmber, String address,
			long level, long vipLevel, long needExperience, long nowExperience, long vipNeedExperience,
			long vipNowExperience, int total_rechage, String loginType) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.question = question;
		this.answer = answer;
		this.email = email;
		this.telphone = telphone;
		this.face = face;
		this.wechat = vchate;
		this.qqNumber = qqNumber;
		this.birthday = birthday;
		this.alipay = alipay;
		this.bankCard = bankCard;
		this.realname = realname;
		this.nickname = nickname;
		this.registerTime = registerTime;
		this.registerIp = registerIp;
		this.gold = gold;
		this.idcardNUmber = idcardNUmber;
		this.address = address;
		this.level = level;
		this.vipLevel = vipLevel;
		this.needExperience = needExperience;
		this.nowExperience = nowExperience;
		this.vipNeedExperience = vipNeedExperience;
		this.vipNowExperience = vipNowExperience;
		this.total_rechage = total_rechage;
		this.loginType = loginType;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getTelphone() {
		return telphone;
	}

	public void setTelphone(long telphone) {
		this.telphone = telphone;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechate) {
		this.wechat = wechate;
	}

	public String getQqNumber() {
		return qqNumber;
	}

	public void setQqNumber(String qqNumber) {
		this.qqNumber = qqNumber;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getBankCard() {
		return bankCard;
	}

	public void setBankCard(String bankCard) {
		this.bankCard = bankCard;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public long getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(long registerTime) {
		this.registerTime = registerTime;
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public long getGold() {
		return gold;
	}

	public void setGold(long gold) {
		this.gold = gold;
	}

	public String getIdcardNUmber() {
		return idcardNUmber;
	}

	public void setIdcardNUmber(String idcardNUmber) {
		this.idcardNUmber = idcardNUmber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getLevel() {
		return level;
	}

	public void setLevel(long level) {
		this.level = level;
	}

	public long getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(long vipLevel) {
		this.vipLevel = vipLevel;
	}

	public long getNeedExperience() {
		return needExperience;
	}

	public void setNeedExperience(long needExperience) {
		this.needExperience = needExperience;
	}

	public long getNowExperience() {
		return nowExperience;
	}

	public void setNowExperience(long nowExperience) {
		this.nowExperience = nowExperience;
	}

	public long getVipNeedExperience() {
		return vipNeedExperience;
	}

	public void setVipNeedExperience(long vipNeedExperience) {
		this.vipNeedExperience = vipNeedExperience;
	}

	public long getVipNowExperience() {
		return vipNowExperience;
	}

	public void setVipNowExperience(long vipNowExperience) {
		this.vipNowExperience = vipNowExperience;
	}

	public int getTotal_rechage() {
		return total_rechage;
	}

	public void setTotal_rechage(int total_rechage) {
		this.total_rechage = total_rechage;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	
}
