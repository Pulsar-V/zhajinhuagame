package org.xiaojia.redis;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.xiaojia.beans.Room;
import org.xiaojia.utils.RedisUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;

/**
 * 房间数据的创建与读取
 * @author XiaoJia
 *
 */
public class RoomDao {
	private ShardedJedis jedis;

	public RoomDao() {
		this.jedis = RedisUtil.getInstance().getSharedJedis();
	}

	public Map<String, String> GetRoom(String key) {
		Map<String, String> map =jedis.hgetAll(key);
		return map;
	}

	public Set<String> GetRoomsBykey(String key) {
		Jedis jedis1 = RedisUtil.getInstance().getJedis();
		return jedis1.keys(key);
	}

	/**
	 * 创建房间
	 * 
	 * @param key
	 * @param room
	 * @return
	 */
	public boolean setRoom(String key, Room room) {// 创建房间
		Map<String, String> hash = new HashMap<String, String>();
		// 设置用户id
		hash.put("uid", "" + room.getUid());
		// 设置房间id
		hash.put("id", room.getId());
		// 房间是否加密
		hash.put("islock", "" + room.isLock());
		// 设置房间密码，如果不密，密码为空
		hash.put("password", room.getPassword());
		// 设置房间庄家
		hash.put("banker", room.getBanker());
		// 设置房间类型
		hash.put("type", room.getType());
		// 设置房主id
		hash.put("masterUid", room.getMasterUid());
		// 设置房间人数
		hash.put("state", room.getState());

		if ("OK".equals(jedis.hmset(key, hash))) {
			return true;
		}
		// 回收资源
		jedis.close();
		return false;
	}

	
	/**
	 * 解散房间 
	 * @param key
	 * @return long 删除成功返回1，删除失败返回0
	 */
	public long destroyRoom(String key) {
		return jedis.del(key);
	}

	/**
	 * 加入房间
	 * 
	 * @param key
	 * @param password
	 * @return Map<String, Object> 返回状态和错误提示
	 */
	public Map<String, Object> addRoom(String key, String password,String userId) {
		String str = jedis.hget(key, "password");
		String uid = jedis.hget(key, "uid");
		int state = Integer.valueOf(jedis.hget(key, "state"));
		Map<String, Object> result = new HashMap<String,Object>();
		// 先判断房间是否满人
		if (uid.split(",").length == state) {
			result.put("message", "房间满人");
		}

		// 判断房间密码是否正确
		if (!str.equals(password)) {
			result.put("message", "房间密码不正确");
		}
		
		//添加用户
		long ret=jedis.hset(key, "uid", uid+","+userId);
		if(ret==1){
			result.put("state","success");
		}else{
			result.put("state", "failed");
		}
		return result;
		
	}

}
