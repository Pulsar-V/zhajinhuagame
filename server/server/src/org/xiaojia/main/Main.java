package org.xiaojia.main;

import org.xiaojia.service.SocketServer;

/**
 * 服务端程序
 * @author XiaoJia
 *
 */
public class Main {
	
	public static void main(String[] args) {
		new SocketServer().start();
	}
}
