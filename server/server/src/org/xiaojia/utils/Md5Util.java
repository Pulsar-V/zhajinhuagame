package org.xiaojia.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Encoder;

/**
 * md5加密类
 * @author XiaoJia
 *
 */
@SuppressWarnings("restriction")
public class Md5Util {
	public static String md5(String str) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("md5");
			BASE64Encoder base64en = new BASE64Encoder();
			str = base64en.encode(md5.digest(str.getBytes("utf-8")));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
}
