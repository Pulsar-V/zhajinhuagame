package org.xiaojia.utils;

import java.io.IOException;
import java.io.InputStream;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.xiaojia.log.LogerBuild;

/**
 * 操作连接数据库的类，采用饿汉式单例设计模式
 * @author xiaoJia
 *
 */
public class DatabaseUtil {
	
	private static DatabaseUtil instance = new DatabaseUtil();
	private SqlSessionFactory sqlSessionFactory;
	
	private DatabaseUtil(){
		InputStream inputStream = null;
		try
		{
			inputStream = Resources.getResourceAsStream("org/xiaojia/config/mybatis.xml");
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		}catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			if(inputStream!=null){
				try {
					inputStream.close();
				} catch (IOException e) {
					LogerBuild.fatal("资源关闭失败");
				}
			}
		}
	}
	
	public static DatabaseUtil getInstance(){
		return instance;
	}
	
	/**
	 * 获取sqlsession
	 */
	public  SqlSession getSession(){
		return this.sqlSessionFactory.openSession();
	}
	
	/**
	 * 开启事务专用 
	 * @param autoCommit 是否关闭自动提交
	 * @return
	 */
	public  SqlSession getSession(boolean autoCommit){
		return this.sqlSessionFactory.openSession(autoCommit);
	}
	
}
