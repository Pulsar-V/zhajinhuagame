package org.xiaojia.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.xiaojia.log.LogerBuild;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

/**
 * Redis连接池
 * @author XiaoJia
 *
 */
public class RedisUtil {
	/**
	 * 共享线程池
	 */
	private ShardedJedisPool shardedJedisPool;
	/**
	 * 共享jedis实例
	 */
	private ShardedJedis jedis;
	/**
	 * 本类的实例
	 */
	private static RedisUtil instance = new RedisUtil();
	/**
	 * 线程池配置
	 */
	private JedisPoolConfig config = new JedisPoolConfig();

	private RedisUtil() {
		try{
		    InputStream in = ClassLoader.getSystemResourceAsStream("org/xiaojia/config/redis.properties");  
			Properties prop = new Properties();  
			prop.load(in);  
			
			config.setMaxIdle(Integer.valueOf(prop.getProperty(("redis.pool.maxIdle"))));
			config.setTestOnBorrow(Boolean.valueOf(prop.getProperty("redis.pool.testOnBorrow")));
			config.setTestOnReturn(Boolean.valueOf(prop.getProperty("redis.pool.testOnReturn")));
			JedisShardInfo jedisShardInfo1 = new JedisShardInfo(prop.getProperty("redis1.host"),Integer.valueOf(prop.getProperty("redis.port")));
			JedisShardInfo jedisShardInfo2 = new JedisShardInfo(prop.getProperty("redis2.host"),Integer.valueOf(prop.getProperty("redis.port")));
	
			jedisShardInfo1.setPassword(prop.getProperty("redis.password"));
			jedisShardInfo2.setPassword(prop.getProperty("redis.password"));
			
			
			List<JedisShardInfo> list = new LinkedList<JedisShardInfo>();
			list.add(jedisShardInfo1);
			list.add(jedisShardInfo2);
	
			// 根据配置文件,创建shared池实例
			shardedJedisPool = new ShardedJedisPool(config, list);
			jedis = shardedJedisPool.getResource();
		}catch (Exception e) {
			LogerBuild.fatal(e.getMessage());
		}
	}

	public static RedisUtil getInstance() {
		return instance;
	}

	public ShardedJedisPool getShardedJedisPool() {
		return shardedJedisPool;
	}

	public ShardedJedis getSharedJedis() {
		return jedis;
	}

	public Jedis getJedis() {
		InputStream in = ClassLoader.getSystemResourceAsStream("org/xiaojia/config/redis.properties");  
		Properties prop = new Properties();  
		Jedis jedis = null;
		try {
			prop.load(in);  
			String host = prop.getProperty("redis1.host");
			int port=  Integer.parseInt(prop.getProperty("redis.port"));
			jedis = new Jedis(host, port);
			jedis.auth(prop.getProperty("redis.password"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jedis;
	}

}
