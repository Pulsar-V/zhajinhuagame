package org.xiaojia.utils;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSourceFactory;

/**
 * 用于数据库连接池的配置
 * @author XiaoJia
 *
 */
public class DBCPDataSource extends UnpooledDataSourceFactory{
	public DBCPDataSource() {
	    this.dataSource = new BasicDataSource();
	  }
}
