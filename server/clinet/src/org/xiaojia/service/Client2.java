package org.xiaojia.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.xiaojia.beans.Room;
import org.xiaojia.beans.User;
import org.xiaojia.rsa.RSA;
import org.xiaojia.utils.CommandTransfer;

public class Client2 {
	Socket socket = null;// 客户端socket
	Scanner sc = new Scanner(System.in);
	User user = null;

	public void showMenu() {
		System.out.println("**************欢迎使用XiaoJia的登录程序************");
		System.out.println("注册用户：1 \n用户登录：2 \n 退出：3\n4 getuserinfo\n5 创建房间\n6 enterhell");
		System.out.println("*****************************************");
		System.out.print("请选择：");
		switch (sc.nextInt()) {
		case 1:
			doRegister();
			break;
		case 2:
			doLogin();
			break;
		case 3:
			System.out.println("谢谢，再见！");
			System.exit(0);
		case 4:
			doGetUserInfo();
			break;
		case 5:
			doCreateRoom();
			break;
		case 6:
			enterhell();
			break;
		default:
			System.out.println("输入有误！");
			break;
		}

	}

	private void enterhell() {
		CommandTransfer commandTransfer = new CommandTransfer();
		Map<String, String> hash = new HashMap<String, String>();
		hash.put("sessionhash", "76TsIWhx9uvQ72k/CaAwOg==");
		hash.put("uid", "3");
		hash.put("type", "primary");
		
		commandTransfer.setData(hash);
		commandTransfer.setCmd("enterhell");
		try {
			socket = new Socket("localhost", 8888);
			sendData(commandTransfer);
			commandTransfer = getData();
			System.out.println(commandTransfer.getMapResult());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void doCreateRoom() {
		CommandTransfer commandTransfer = new CommandTransfer();
		String uid = "1,2,3,4,5";
		boolean lock = false;
		String password = "";
		String banker = "";
		String type = "5";
		String id = "23123123123";
		String masterUid = "5";
		String state = "5";
		Room room = new Room(uid, lock, password, banker, type, id, masterUid, state);
		commandTransfer.setData(room);
		commandTransfer.setCmd("room");

		try {
			socket = new Socket("localhost", 8888);
			sendData(commandTransfer);
			commandTransfer = getData();
			System.out.println(commandTransfer.getMapResult());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void doGetUserInfo() {
		CommandTransfer commandTransfer = new CommandTransfer();
		HashMap<String, Object> hash = new HashMap<String, Object>();
		hash.put("sessionhash", "76TsIWhx9uvQ72k/CaAwOg==");
		hash.put("uid", "3");
		commandTransfer.setData(hash);
		commandTransfer.setCmd("getuserinfo");

		try {
			socket = new Socket("localhost", 8888);
			sendData(commandTransfer);
			commandTransfer = getData();
			System.out.println(commandTransfer.getMapResult());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}

	private void doLogin() {
		user = new User();
		CommandTransfer commandTransfer = new CommandTransfer();
		RSA rsaClient = new RSA();
		try {
			rsaClient.loadPublicKeyFile("pub.key");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		int count = 0;
		while (true) {
			count++;
			// 三次登录
			if (count > 3) {
				System.out.println("抱歉，您已经用完三次机会，请下次登录！");
				System.exit(0);
			}
			System.out.print("请输入用户名：");
			user.setUsername(rsaClient.encryptData(sc.next()));
			System.out.print("请输入密码：");
			user.setPassword(rsaClient.encryptData(sc.next()));
			user.setLoginType("normal");
			commandTransfer.setData(user);
			commandTransfer.setCmd("login");
			try {
				socket = new Socket("localhost", 8888);
				sendData(commandTransfer);
				commandTransfer = getData();

				// System.out.println(commandTransfer.getResult());
				System.out.println(commandTransfer.getMapResult());
				System.out.println("**************************");

				if (commandTransfer.isFlag()) {
					break;
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	private void doRegister() {
		user = new User();
		CommandTransfer commandTransfer = new CommandTransfer();

		while (true) {
			System.out.print("请输入用户名：");
			user.setUsername(sc.next());
			System.out.print("请输入密码：");
			user.setPassword(sc.next());
			System.out.print("请再次确认密码：");
			String rePassword = sc.next();
			System.out.print("请输入昵称：");
			user.setNickname(sc.next());
			System.out.print("请输入邮箱：");
			user.setEmail(sc.next());
			System.out.print("请输入电话：");
			user.setTelphone(sc.nextLong());

			if (!rePassword.equals(user.getPassword())) {
				System.out.println("**********两次输入的密码不一致***********");
				continue;
			}
			commandTransfer.setCmd("register");
			commandTransfer.setData(user);
			try {
				socket = new Socket("localhost", 8888);
				sendData(commandTransfer);// 发送数据给服务器
				commandTransfer = getData();// 获取服务器返回的数据
				System.out.println(commandTransfer.getResult());
				System.out.println("***************************");
				if (commandTransfer.isFlag()) {
					break;// 如果注册成功，则不再重复注册
				}

			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		doLogin();
	}

	/**
	 * 获取服务器返回的数据
	 * 
	 * @return
	 */
	private CommandTransfer getData() {
		InputStream is;
		ObjectInputStream osi;
		CommandTransfer commandTransfer = null;
		try {
			is = socket.getInputStream();
			osi = new ObjectInputStream(is);
			commandTransfer = (CommandTransfer) osi.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return commandTransfer;
	}

	/**
	 * 向服务器发送数据
	 * 
	 * @param commandTransfer
	 */
	private void sendData(CommandTransfer commandTransfer) {
		try {
			OutputStream os = socket.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(commandTransfer);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}