package org.xiaojia.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONPointer;
import org.json.JSONTokener;
import org.xiaojia.beans.Room;
import org.xiaojia.beans.User;
import org.xiaojia.rsa.RSA;

public class Client {
	private Socket socket = null;// 客户端socket
	private Scanner sc = new Scanner(System.in);
	private User user = null;
	private RSA rsaClient = new RSA();

	public Client(){
		try {
			rsaClient.loadPublicKeyFile("pub.key");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	
	public void showMenu() {
		System.out.println("**************欢迎使用XiaoJia的登录程序************");
		System.out.println("注册用户：1 \n用户登录：2 \n 退出：3\ngetuserinfo：4\n创建房间：5 \nenterhell:6");
		System.out.println("*****************************************");
		System.out.print("请选择：");
		switch (sc.nextInt()) {
		case 1:
			doRegister();
			break;
		case 2:
			doLogin();
			break;
		case 3:
			System.out.println("谢谢，再见！");
			System.exit(0);
		case 4:
			doGetUserInfo();
			break;
		case 5:
			doCreateRoom();
			break;
		case 6:
			enterhell();
			break;
		default:
			System.out.println("输入有误！");
			break;
		}

	}

	private void enterhell() {
		Map<String, String> hash = new HashMap<String, String>();
		hash.put("sessionhash", (String)rsaClient.encryptData("76TsIWhx9uvQ72k/CaAwOg=="));
		hash.put("uid", (String)rsaClient.encryptData("3"));
		hash.put("type", (String)rsaClient.encryptData("primary"));
		hash.put("cmd", (String)rsaClient.encryptData("enterhell"));

		JSONObject jsonObject = new JSONObject(hash);
		try {
			socket = new Socket("localhost", 8888);
			sendData(jsonObject);
			JSONObject result = getData();
			System.out.println(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void doCreateRoom() {
		String uid = "1,2,3,4,5";
		boolean lock = false;
		String password = "";
		String banker = "";
		String type = "5";
		String id = "23123123123";
		String masterUid = "5";
		String state = "5";
		Room room = new Room(uid, lock, password, banker, type, id, masterUid, state);

		JSONObject jsonObject = new JSONObject(room);
		jsonObject.append("cmd", (String)rsaClient.encryptData("createRoom"));

		try {
			socket = new Socket("localhost", 8888);
			sendData(jsonObject);
			JSONObject result = getData();
			System.out.println(result.toString());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void doGetUserInfo() {
		HashMap<String, Object> hash = new HashMap<String, Object>();
		hash.put("sessionhash", (String)rsaClient.encryptData("76TsIWhx9uvQ72k/CaAwOg=="));
		hash.put("uid", (String)rsaClient.encryptData("3"));
		hash.put("cmd", (String)rsaClient.encryptData("getuserinfo"));

		JSONObject jsonObject = new JSONObject(hash);
		try {
			socket = new Socket("localhost", 8888);
			sendData(jsonObject);
			JSONObject result = getData();
			System.out.println(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}

	private void doLogin() {
		user = new User();
		
		JSONObject jsonObject = new JSONObject();
		System.out.print("请输入用户名：");
		String username = rsaClient.encryptData(sc.next());
		jsonObject.put("username", username);
		System.out.print("请输入密码：");
		String password = rsaClient.encryptData(sc.next());
		jsonObject.put("password",password);
		String login = rsaClient.encryptData("login");
		jsonObject.put("cmd", login);
		String normal = rsaClient.encryptData("normal");
		jsonObject.put("type", normal);
		
		System.out.println(rsaClient.getPrivateExponent());
		
		System.out.print(username);
		
		try {
			socket = new Socket("localhost", 8888);
			sendData(jsonObject);
			JSONObject result = getData();
			System.out.println(result.toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void doRegister() {
		user = new User();

		System.out.print("请输入用户名：");
		user.setUsername(sc.next());
		System.out.print("请输入密码：");
		user.setPassword(sc.next());
		System.out.print("请再次确认密码：");
		String rePassword = sc.next();
		System.out.print("请输入昵称：");
		user.setNickname(sc.next());
		System.out.print("请输入邮箱：");
		user.setEmail(sc.next());
		System.out.print("请输入电话：");
		user.setTelphone(sc.nextLong());

		if (!rePassword.equals(user.getPassword())) {
			System.out.println("**********两次输入的密码不一致***********");
			return ;
		}
		JSONObject jsonObject = new JSONObject(user);
		jsonObject.append("cmd", rsaClient.encryptData("register"));
		try {
			socket = new Socket("localhost", 8888);
			sendData(jsonObject);// 发送数据给服务器
			JSONObject result = getData();// 获取服务器返回的数据
			System.out.println(result.toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		doLogin();
	}

	/**
	 * 获取服务器返回的数据
	 * 
	 * @return
	 */
	private JSONObject getData() {
		InputStream is = null;
		JSONObject object = null;
		try {
			is = socket.getInputStream();
			byte[] buf = new byte[1];
			StringBuffer stringBuffer = new StringBuffer();
			while (is.read(buf) != -1) {
				if (buf[0] == "#".getBytes()[0]) {
					break;
				}
				stringBuffer.append(new String(buf));
			}
			object = new JSONObject(stringBuffer.toString());
		} catch (Exception e) {
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return object;
	}

	/**
	 * 向服务器发送数据
	 * 
	 * @param commandTransfer
	 */
	private void sendData(JSONObject jsonObject) {
		OutputStream os = null;
		BufferedWriter bufferedWriter = null;
		try {
			os = socket.getOutputStream();
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			bufferedWriter.write(jsonObject.toString() + "#");
			bufferedWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}