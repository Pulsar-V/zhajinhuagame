package org.xiaojia.utils;

import java.io.Serializable;
import java.util.Map;  
/** 
 * 表示客户机和服务器之间传输的指令数据 
 * @author xiaojia
 * 
 */  
public class CommandTransfer implements Serializable{  
  
    private static final long serialVersionUID = 1L;  
      
    private Object data; 
    private String result;
    private Map<String, Object> MapResult; 
    private boolean flag;
    private String cmd;  
    
    public CommandTransfer() {  
        super();  
    }  
    
    public Object getData() {  
        return data;  
    }  
    public void setData(Object data) {  
        this.data = data;  
    }  
    public String getResult() {  
        return result;  
    }  
    public void setResult(String result) {  
        this.result = result;  
    }  
    public boolean isFlag() {  
        return flag;  
    }  
    public void setFlag(boolean flag) {  
        this.flag = flag;  
    }  
    public String getCmd() {  
        return cmd;  
    }  
    public void setCmd(String cmd) {  
        this.cmd = cmd;  
    }

	public Map<String, Object> getMapResult() {
		return MapResult;
	}

	public void setMapResult(Map<String, Object> mapResult) {
		MapResult = mapResult;
	}  
}  