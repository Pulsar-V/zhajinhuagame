package org.xiaojia.main;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Test {
	public static void main(String[] args) {
		
		Socket socket;
		try {
			socket = new Socket("localhost", 8888);
			OutputStream os = socket.getOutputStream();
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write("{\"cmd\":\"getuserinfo\"}"+"\n"+"\r\n");
			bw.flush();
			bw.close();
			os.close();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}
