package org.xiaojia.main;

import org.xiaojia.service.Client;

/**
 * 客户端程序
 * @author XiaoJia
 *
 */
public class Main {
	public static void main(String[] args) {  
        Client client = new Client();  
        client.showMenu();  
    }  
}
