package org.xiaojia.beans;

import java.sql.Time;

/**
 * 好友列表的实体类
 * @author XiaoJia
 *
 */
public class Friend {
	/**
	 * 用户id
	 */
	private int uid;
	/**
	 * 添加好友时间
	 */
	private Time addTime;
	/**
	 * 这个部分其实计数present表的to_uid即可
	 */
	private int presentNum;
	/**
	 * 好友ID
	 */
	private int friendsId;

	public Friend(){
		
	}
	
	public Friend(int uid, Time addTime, int presentNum, int friendsId) {
		super();
		this.uid = uid;
		this.addTime = addTime;
		this.presentNum = presentNum;
		this.friendsId = friendsId;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public Time getAddTime() {
		return addTime;
	}
	public void setAddTime(Time addTime) {
		this.addTime = addTime;
	}
	public int getPresentNum() {
		return presentNum;
	}
	public void setPresentNum(int presentNum) {
		this.presentNum = presentNum;
	}
	public int getFriendsId() {
		return friendsId;
	}
	public void setFriendsId(int friendsId) {
		this.friendsId = friendsId;
	}
}
