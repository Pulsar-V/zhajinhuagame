package org.xiaojia.beans;

import java.util.Date;

/**
 * 游戏历史记录
 * @author XiaoJia
 *
 */
public class History {
	private int id;
	private int victoryId;
	private Date createTime;
	private int money;
	private long experience;
	private int time_consumig;
	
	public History(){
		
	}
	
	public History(int id, int victoryId, Date createTime, int money, long experience, int time_consumig) {
		super();
		this.id = id;
		this.victoryId = victoryId;
		this.createTime = createTime;
		this.money = money;
		this.experience = experience;
		this.time_consumig = time_consumig;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVictoryId() {
		return victoryId;
	}
	public void setVictoryId(int victoryId) {
		this.victoryId = victoryId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public long getExperience() {
		return experience;
	}
	public void setExperience(long experience) {
		this.experience = experience;
	}
	public int getTime_consumig() {
		return time_consumig;
	}
	public void setTime_consumig(int time_consumig) {
		this.time_consumig = time_consumig;
	}
}
