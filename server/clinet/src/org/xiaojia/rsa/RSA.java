package org.xiaojia.rsa;

/**
 * Created by DKing on 2017/7/11.
 */


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Random;


@SuppressWarnings("restriction")
public class RSA {

    private PublicKey publicKey = null;
    private PrivateKey privateKey = null;
    private static Cipher cipher = null;
    private static  KeyFactory keyFactory = null;
    private RSAPublicKeySpec rsaPubKeySpec = null;
    private RSAPrivateKeySpec rsaPrivKeySpec = null;

    static
    {
        try {
            cipher = Cipher.getInstance("RSA");
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }


    public static void main(String argv[]) throws IOException {
        RSA rsa = new RSA();
        rsa.createKeys();
        System.out.println(rsa.getPrivateExponent());
        System.out.println(rsa.getPublicExponent());
        System.out.println(rsa.getModulus());
    }


    public void createKeys() throws IOException {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            BigInteger pubkey_bignum = BigInteger.probablePrime(  2048, new Random());
            RSAKeyGenParameterSpec keygenSpec = new RSAKeyGenParameterSpec(2048, pubkey_bignum);
            keyPairGenerator.initialize(keygenSpec);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            publicKey = keyPair.getPublic();
            privateKey = keyPair.getPrivate();
            rsaPubKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
            rsaPrivKeySpec = keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    public boolean savePublicKeyFile(String fileName) throws IOException {
        return saveKeyFile(fileName, rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
    }

    public boolean savePrivateKeyFile(String fileName) throws IOException {
        return saveKeyFile(fileName, rsaPrivKeySpec.getModulus(), rsaPrivKeySpec.getPrivateExponent());
    }

    public String getPrivateExponent()
    {
        if(rsaPrivKeySpec!=null)
            return rsaPrivKeySpec.getPrivateExponent().toString();
        else
            return "";
    }

    public String getPublicExponent()
    {
        if(rsaPubKeySpec!=null)
            return rsaPubKeySpec.getPublicExponent().toString();
        else
            return "";
    }

    public String getModulus()
    {
        if(rsaPrivKeySpec!=null) return rsaPrivKeySpec.getModulus().toString();
        if(rsaPubKeySpec!=null) return rsaPubKeySpec.getModulus().toString();
        return "";
    }


    public void setPrivateKey(String Exponent,String Modulus) throws InvalidKeySpecException
    {
        BigInteger modulus = new BigInteger(Modulus);
        BigInteger exponent = new BigInteger(Exponent);

        rsaPrivKeySpec = new RSAPrivateKeySpec(modulus, exponent);
        privateKey = keyFactory.generatePrivate(rsaPrivKeySpec);

    }

    public void setPublicKey(String Exponent,String Modulus) throws InvalidKeySpecException
    {
        BigInteger modulus = new BigInteger(Modulus);
        BigInteger exponent = new BigInteger(Exponent);

        rsaPubKeySpec = new RSAPublicKeySpec(modulus, exponent);
        publicKey = keyFactory.generatePublic(rsaPubKeySpec);
    }


    private boolean saveKeyFile(String fileName,BigInteger mod,BigInteger exp) throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(fileName);
            oos = new ObjectOutputStream(new BufferedOutputStream(fos));

            oos.writeObject(mod);
            oos.writeObject(exp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally{
            if(oos != null)oos.close();
            if(fos != null)fos.close();
        }
    }


    public String encryptData(String data) {
        byte[] dataToEncrypt = data.getBytes();
        byte[] encryptedData = null;
        String enc_data = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encryptedData = cipher.doFinal(dataToEncrypt);
            enc_data = new BASE64Encoder().encode(encryptedData);
        } catch (Exception e) {
            enc_data = "";
            e.printStackTrace();
        }
        return enc_data;
    }


    public String decryptData(String base64_data){
        byte[] descryptedData = null;
        byte[] databyte = null;
        String dec_data = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            databyte = new BASE64Decoder().decodeBuffer(base64_data);
            descryptedData = cipher.doFinal(databyte);
            dec_data = new String(descryptedData);
        } catch (Exception e) {
            dec_data = "";
            e.printStackTrace();
        }
        return dec_data;

    }

    public void loadPublicKeyFile(String fileName) throws IOException{
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(new File(fileName));
            ois = new ObjectInputStream(fis);

            BigInteger modulus = (BigInteger) ois.readObject();
            BigInteger exponent = (BigInteger) ois.readObject();

            rsaPubKeySpec = new RSAPublicKeySpec(modulus, exponent);
            publicKey = keyFactory.generatePublic(rsaPubKeySpec);


        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if(ois != null)ois.close();
            if(fis != null)fis.close();
        }
    }

    public void loadPrivateKeyFile(String fileName) throws IOException{
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(new File(fileName));
            ois = new ObjectInputStream(fis);

            BigInteger modulus = (BigInteger) ois.readObject();
            BigInteger exponent = (BigInteger) ois.readObject();

            rsaPrivKeySpec = new RSAPrivateKeySpec(modulus, exponent);
            privateKey = keyFactory.generatePrivate(rsaPrivKeySpec);

        } catch (Exception e) {
            e.printStackTrace();
        }

        finally{
            if(ois != null)ois.close();
            if(fis != null)fis.close();
        }
    }
}

