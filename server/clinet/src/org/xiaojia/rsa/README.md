#加密方案：

##Step1：服务端使用RSA，向客户端分配会话密钥session_key。使用效率高的对称算法以session_key为参数加解密整个会话。


####1.初次生成两队密钥对，分别给Server和Client使用。
```
RSA rsa = new RSA();
rsa.createKeys(2048);
rsa.savePrivateKeyFile("priv.key");
rsa.savePublicKeyFile("pub.key");

Modulus = rsa.getModulus()
PrivateExponent = getPrivateExponent()
PublicExponent = getPublicExponent()
```
####2.然后将 `String(Modulus,Exponent)` 或 `priv.key` 保留在客户端，混淆。

####3.session_key会话密钥 每次分配：服务端 -> 客户端: 
发送：
```
RSA rsa_server = new RSA();
rsa_server.loadPublicKeyFile("s_pub.key");
Session_Key_ENC = rsa_server.encryptData(session_key)
```
接收：
```
RSA rsa_client = new RSA();
rsa_client.setPrivateKey(PrivateExponent , Modulus )
session_key = rsa_client.decryptData(Session_Key_ENC)
```

##Step2： 使用异或或其他算法，session_key加解密之后的整个通信



